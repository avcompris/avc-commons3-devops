# avc-commons3-devops

This project builds two Docker images:

* `avcompris/maven-plus-docker`, used to run Docker containers alongside
  JUnit tests 
* `avcompris/git-ssh`, used to tag “docker_builds” branches from Git
  repositories